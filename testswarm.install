<?php

/**
 * @file
 *   TestSwarm module install/schema hooks.
 */

/**
 * Implements hook_schema().
 */
function testswarm_schema() {
  $schema = array();

  $schema['testswarm_info'] = array(
    'description' => 'Table for storing info about tests',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'caller' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'description' => array(
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
      ),
      'githash' => array(
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
        'default' => '',
      ),
      'sitename' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'url' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'version' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => FALSE,
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'testswarm_caller_unique' => array('caller', 'module', 'version', 'sitename', 'url'),
    ),
    'primary key' => array('id'),
  );

  $schema['testswarm_test'] = array(
    'description' => 'Table for tests.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'info_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'theme' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'useragent' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'total' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'passed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'failed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'runtime' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'karma' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'info_id' => array('info_id'),
      'theme' => array('theme'),
      'karma' => array('karma'),
    ),
    'foreign keys' => array(
      'testswarm_test_info' => array(
        'table' => 'testswarm_info',
        'columns' => array('info_id' => 'id'),
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['testswarm_test_run'] = array(
    'description' => 'Table for storing test runs.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'qt_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'total' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'passed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'failed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['testswarm_test_run_detail'] = array(
    'description' => 'Table for storing test run details.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'tri' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'result' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'message' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'actual' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'expected' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Add githash column.
 */
function testswarm_update_7001() {
  $githash = array(
    'githash' => array(
      'type' => 'varchar',
      'length' => 40,
      'not null' => FALSE,
    ),
  );

  db_add_field('testswarm_test', 'githash', $githash['githash']);
}

/**
 * Add theme column.
 */
function testswarm_update_7002() {
  $theme = array(
    'theme' => array(
      'type' => 'varchar',
      'length' => 150,
      'not null' => FALSE,
    ),
  );

  db_add_field('testswarm_test', 'theme', $theme['theme']);
  db_query("UPDATE {testswarm_test} set theme = 'bartik'");
}

/**
 * Add karma column.
 */
function testswarm_update_7003() {
  $karma = array(
    'karma' => array(
      'type' => 'varchar',
      'length' => 50,
      'not null' => FALSE,
    ),
  );

  db_add_field('testswarm_test', 'karma', $karma['karma']);
  db_query("UPDATE {testswarm_test} set karma = 'jane doe'");
}

/**
 * Add indexes.
 */
function testswarm_update_7004() {
  db_add_index('testswarm_test', 'caller', array('caller'));
  db_add_index('testswarm_test', 'githash', array('githash'));
  db_add_index('testswarm_test', 'theme', array('theme'));
  db_add_index('testswarm_test', 'karma', array('karma'));
}

/**
 * Split testswarm_test table into testswarm_test and testswarm_info.
 */
function testswarm_update_8001() {
  $transaction = db_transaction();
  try {
    $schema = testswarm_schema();

    // Create the new table.
    db_create_table('testswarm_info', $schema['testswarm_info']);

    // Add the field that will contain the foreign key.
    db_add_field('testswarm_test', 'info_id', $schema['testswarm_test']['fields']['info_id']);
    db_add_index('testswarm_test', 'info_id', $schema['testswarm_test']['indexes']['info_id']);

    global $base_url;
    $version = explode('.', \Drupal::VERSION);
    $version = array_shift($version);
    $sitename = config('system.site')->get('name');

    // Shift the data to the new table.
    $query = db_select('testswarm_test', 'tt')->fields('tt', array('caller', 'githash'));
    $query->addExpression($version, 'version');
    $query->addExpression("'{$sitename}'", 'sitename');
    $query->addExpression("'{$base_url}'", 'url');
    $query->addExpression(REQUEST_TIME, 'timestamp');
    $query->addExpression("''", 'description');
    $query->groupBy('tt.caller');
    $query->groupBy('version');
    db_insert('testswarm_info')->from($query)->execute();

    // Update the data in the new table with the correct values.
    foreach (testswarm_defined_tests() as $test) {
      db_update('testswarm_info')
        ->fields(array('module' => $test['module'], 'description' => $test['description']))
        ->condition('caller', $test['caller'])
        ->execute();
    }

    // Set the foreign keys.
    db_query("
      UPDATE {testswarm_test} tt
      SET info_id = (
        SELECT id
        FROM {testswarm_info} ti
          WHERE tt.caller = ti.caller AND tt.githash = ti.githash
      )
    ");

    // Remove records with no foreign keys.
    db_delete('testswarm_test')->condition('info_id', 0)->execute();

    // Clean up the table.
    db_drop_index('testswarm_test', 'caller');
    db_drop_index('testswarm_test', 'githash');
    db_drop_field('testswarm_test', 'caller');
    db_drop_field('testswarm_test', 'githash');
  }
  catch (Exception $e) {
    $transaction->rollback();
    throw $e;
  }
}
